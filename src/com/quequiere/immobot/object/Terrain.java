package com.quequiere.immobot.object;

import java.util.ArrayList;

public class Terrain
{
	public static ArrayList<Terrain> liste = new ArrayList<Terrain>();
	public String name;
	public int prix;
	private int id=-1;
	
	
	public Terrain(String name, int prix, int id2)
	{
		this.name = name;
		this.prix = prix;
		this.id=id2;
	}
	
	public String getName()
	{
		return name;
	}
	public int getPrix()
	{
		return prix;
	}

	public int getId()
	{
		return id;
	}
	
	
	

}
