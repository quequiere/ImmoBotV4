package com.quequiere.immobot.object;

public enum BatimentTypeEntreprise
{
	bureaux,
	commerciaux,
	industriels,
	transport,
	zones;
}
