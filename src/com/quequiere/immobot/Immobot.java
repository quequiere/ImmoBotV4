package com.quequiere.immobot;

import java.text.Normalizer;
import java.util.ArrayList;

import com.quequiere.immobot.object.Profile;
import com.quequiere.immobot.runnable.BuyerWorker;

public class Immobot
{

	public static Profile p;
	public static long scanMin = 30;
	public static long minMoney = 10000000;
	
	public static ArrayList<Integer> sellList = new ArrayList<Integer>();
	public static int buyId  = 0;

	public static void main(String... args)
	{
		System.out.println("Starting bot ....");

		if (args.length < 4)
		{
			System.out.println("Use:");
			System.out.println("-user username");
			System.out.println("-psw password");
			System.out.println("-money minMoney (fac)");
			System.out.println("-scan minuteScan (fac)");
			System.out.println("-sell id;id;id");
			System.out.println("-buy id");
			return;

		}

		String user = "";
		String psw = "";

		for (int x = 0; x < args.length; x++)
		{
			String current = args[x];
			String next = "";
			if (x + 1 != args.length)
			{
				next = args[x + 1];
			}

			switch (current) {
			case "-user":
				user = next;
				break;
			case "-psw":
				psw = next;
				break;
			case "-money":
				minMoney = Long.parseLong(next);
				System.out.println("Min money set to: "+minMoney);
				break;
			case "-scan":
				scanMin = Long.parseLong(next);
				System.out.println("Scan min set to: "+scanMin);
				break;
			case "-sell":
				String[] tab = next.split(";");
				for(String s:tab)
				{
					sellList.add(Integer.parseInt(s));
				}
				break;
			case "-buy":
				buyId=Integer.parseInt(next);
				break;
			}

		}
		if (user.equals("") || psw.equals(""))
		{
			System.out.println("You need to give user and password !");
			return;
		}
		p = new Profile(user, psw, false);
		p.connect();
		p.loadBatiment();
		System.out.println("Batiment loaded: "+Immobot.p.getBatiments().size());
		
		try
		{
			System.out.println("Pause avant demarage ...");
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		launchBot();
	}

	
	public static String cleanString(String s)
	{
		String s2 = s.replace(" ", "");
		s2 = s2.replace("\r", "");
		s2 = s2.replace("\n", "");
		s2 = s2.replace("\t", "");
		return s2;
	}

	static public String sansAccents(String s)
	{

		s = Normalizer.normalize(s, Normalizer.Form.NFD);
		s = s.replaceAll("[^\\p{ASCII}]", "");
		return s;
	}

	public static void launchBot()
	{
		Thread t = new Thread(new BuyerWorker());
		t.start();
	}
	


}
