package com.quequiere.immobot.runnable;

import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.quequiere.immobot.Immobot;
import com.quequiere.immobot.object.Batiment;
import com.quequiere.immobot.object.Terrain;

public class BuyerWorker implements Runnable
{
	public static long nextScan = 0;
	
	
	@Override
	public void run()
	{
		while (true)
		{
			try
			{
				Thread.sleep(250);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			
			
			long now = System.currentTimeMillis();
			long diff = (nextScan - now) / 1000;
			
			
			int day = (int) TimeUnit.SECONDS.toDays(diff);
			long hours = TimeUnit.SECONDS.toHours(diff) - (day * 24);
			long minute = TimeUnit.SECONDS.toMinutes(diff) - (TimeUnit.SECONDS.toHours(diff) * 60);
			long second = TimeUnit.SECONDS.toSeconds(diff) - (TimeUnit.SECONDS.toMinutes(diff) * 60);

			String s = day + "d " + hours + ":" + minute + ":" + second;

			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
			Date dn = new Date(System.currentTimeMillis());
			String dns = sdf.format(dn);

			System.out.print("\r" + s + "  current: " + dns );

			System.out.flush();
			
			if (now >= nextScan)
			{
				this.startNewScan();
			}
		}
		
	}
	
	public static long getDefaultTimer()
	{
		return Immobot.scanMin*60*1000;
	}

	private void programNextScan()
	{
		Random r = new Random();
		int Low = 80;
		int High = 120;
		long randomNum = r.nextInt(High-Low) + Low;
		System.out.println("Random res: "+randomNum);
		
		double ratioChange = randomNum/100.0;
		
		double nextok = ((getDefaultTimer()*ratioChange + System.currentTimeMillis()));
		
		System.out.println("Next: " + nextScan + " def: " +nextok );

		nextScan = Math.round(nextok);
	}
	
	private void startNewScan()
	{
		System.out.println("=== Starting new scan ===");
		this.programNextScan();
		Immobot.p.connect();
		
		
		System.out.println("============================");
		for(int id:Immobot.sellList)
		{
			this.sellProperties(id);
			System.out.println("============================");
		}
		
		Immobot.p.loadMoney();
		long money = Immobot.p.getMoney();
		

		if(Immobot.buyId>0)
		{
			if(Batiment.getById(Immobot.buyId)==null)
			{
				System.out.println("Can't find batiment for id "+Immobot.buyId);
				return;
			}
			
			// on construit terrain en friche
			constructionRun(Immobot.buyId);
			
			
			Immobot.p.loadMoney();
			money = Immobot.p.getMoney();
			
			constructionBuyer(Immobot.buyId);
			
		
		}
		
		
	}
	
	
	public void constructionRun(int idBatiment)
	{
		Batiment b = Batiment.getById(idBatiment);
		
		if(b==null)
		{
			System.out.println("Can't find batiement "+idBatiment);
			return;
		}
		

		try
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);
			

			params.add(new NameValuePair("action", "search"));
			params.add(new NameValuePair("type", "terrain"));
			params.add(new NameValuePair("status", "1"));
			params.add(new NameValuePair("lifetime", "120"));
			params.add(new NameValuePair("building", ""+b.getTerrain().getId()));
			params.add(new NameValuePair("town", ""));
			params.add(new NameValuePair("level", ""));
			params.add(new NameValuePair("pager_index", "0"));
			params.add(new NameValuePair("pager_size", "1000"));
			params.add(new NameValuePair("sort", "6"));
			params.add(new NameValuePair("asc", "1"));

			HtmlPage page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/properties/ajax.php"), params);
			Immobot.p.webClient.waitForBackgroundJavaScript(2000);

			final HtmlTable table = page.getFirstByXPath("//table");

			if (table == null)
			{
				if (page.asText().contains("Aucune propri"))
				{
					System.out.println("No terrain to build !");
				}

			}
			else
			{
				ArrayList<Integer> liste = new ArrayList<>();
				

				for (int x = 1; x < table.getRowCount() ; x++)
				{
					final HtmlTableRow row = table.getRow(x);
					String valS = row.getCell(0).asXml();
					try
					{
						valS = valS.split("value=\"")[1];
						valS = valS.split("\"/")[0];
						liste.add(Integer.parseInt(valS));
					}
					catch(ArrayIndexOutOfBoundsException e)
					{
						if(!row.getCell(0).asXml().contains("<td/>"))
						{
							System.out.println("Fin de ligne: "+row.getCell(0).asXml());
						}
						
					}
					
				}

				System.out.println("Contruction: " + liste.size());
				
			

				if (liste.size() > 0)
				{
					params = new ArrayList<NameValuePair>(2);

					String ids = "";

					boolean first = true;
					for (Integer id : liste)
					{
						if (first)
						{
							ids = id + "";
							first = false;
						}
						else
						{
							ids += ";" + id;
						}

					}

					params.add(new NameValuePair("id_propriete", ids));
					params.add(new NameValuePair("id_etat", "5"));
					params.add(new NameValuePair("confirmation", "1"));
					params.add(new NameValuePair("data", ""+idBatiment));
					params.add(new NameValuePair("oneEventOnly", "true"));

					page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/properties/properties_action.php"), params);
					Immobot.p.webClient.waitForBackgroundJavaScript(2000);

					if(page.asText().contains("Vos propriétés sont"))
					{
						System.out.println("Construction sucess !");
					}
					else if(page.asText().contains("Vous ne disposez pas"))
					{
						System.out.println("Erreur fond insufisant !");
					}
					else
					{
						System.out.println("ERROR while construction ");
						System.out.println(page.asXml());
					}

				}
			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}

	}
	
	
	
	
	public void constructionBuyer(int id)
	{
	
		long money = Immobot.p.getMoney();

		long depensable = money - Immobot.minMoney;
		System.out.println("Can buy with " + NumberFormat.getNumberInstance(Locale.FRENCH).format(depensable) + " and min money");
		
		
		Batiment b = Batiment.getById(id);
		
		System.out.println("Starting buy "+b.getName());
		
		
		double cost = b.getConstructionTotalCost();
		
		
		double quantity = depensable / cost;
		double quantityFloored = Math.floor(quantity);
		System.out.println("Can buy: " + quantity + " ==> " + quantityFloored+" priceTotal: "+cost);
		

		try
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("module", "list"));
			params.add(new NameValuePair("type", "terrain"));

			HtmlPage page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/agency/agency_ajax.php"), params);
			Immobot.p.webClient.waitForBackgroundJavaScript(2000);
			final HtmlTable table = page.getFirstByXPath("//table");
			
			for (int x = 1; x < table.getRowCount(); x++)
			{
				final HtmlTableRow row = table.getRow(x);
				if (row.getCell(0).asText().contains(b.getTerrain().getName()))
				{
					int dispo = Integer.parseInt(row.getCell(5).asText().replace(" ", ""));
					double tobuy = 0;

					if (dispo != 0)
					{
						if (quantityFloored > dispo)
						{
							tobuy = dispo;
						}
						else
						{
							tobuy = quantityFloored;
						}
					}

					System.out.println("Buyable: " + dispo + "   ||  Will buy: " + tobuy);

					int fibuy = (int) tobuy;
					
					if (fibuy > 0)
					{
						System.out.println("Starting buying "+fibuy+" "+b.getTerrain().getName());
						this.buyBuilding(b.getTerrain().getId(), fibuy);
					}
				}

			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}
		
		constructionRun(Immobot.buyId);

	}
	
	
	public void buyBuilding(int id, int amount)
	{
		Batiment b = Batiment.getById(id);
		Terrain terrain = null;
		
		if(b==null)
		{
			
			for(Terrain t:Terrain.liste)
			{
				if(id==t.getId())
				{
					terrain=t;
					System.out.println("Id solved as terrain");
					break;
				}
			}
			
			if(terrain==null)
			{
				System.out.println("Can't buy for id: "+id);
				return;
			}
			
		}
		
		
		if(terrain==null)
		{
			System.out.println("Buying "+b.getName());
		}
		else
		{
			System.out.println("Buying "+terrain.getName());
		}
		
		
		try
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("module", "purchase"));
			
			if(terrain==null)
			{
				params.add(new NameValuePair("data", b.getId()+"-" + amount + ";"));
			}
			else
			{
				params.add(new NameValuePair("data",terrain.getId()+"-" + amount + ";"));
			}
			
			

			HtmlPage page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/agency/agency_ajax.php"), params);
			Immobot.p.webClient.waitForBackgroundJavaScript(2000);

			if (page.asText().contains("vous ne poss"))
			{
				System.out.println("ERROR !!! Not enough money ! for:" + amount);
			}
			else if (page.asText().contains("Vos achats sont accomplis"))
			{
				System.out.println("Buy success !");
			}
			else
			{
				System.out.println("Unknow error:");
				System.out.println(page.asText());
			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	public void sellProperties(int idBatiment)
	{
		

		Batiment b = Batiment.getById(idBatiment);
		if(b==null)
		{
			System.out.println("Can't find batiment with id "+idBatiment);
			return;
		}
		System.out.println("Try to sell "+b.getName());

		try
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);
			
			params.add(new NameValuePair("action", "search"));
			params.add(new NameValuePair("type", ""));
			params.add(new NameValuePair("status", "1"));
			params.add(new NameValuePair("lifetime", "120"));
			params.add(new NameValuePair("building", ""+b.getId()));
			params.add(new NameValuePair("town", ""));
			params.add(new NameValuePair("level", ""));
			params.add(new NameValuePair("pager_index", "0"));
			params.add(new NameValuePair("pager_size", "10000"));
			params.add(new NameValuePair("sort", "6"));
			params.add(new NameValuePair("asc", "1"));

			HtmlPage page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/properties/ajax.php"), params);
			Immobot.p.webClient.waitForBackgroundJavaScript(2000);

			final HtmlTable table = page.getFirstByXPath("//table");

			if (table == null)
			{
				if (page.asText().contains("Aucune propri"))
				{
					System.out.println("Page vide pour: "+b.getName());
				}

			}
			else
			{
				ArrayList<Integer> liste = new ArrayList<>();

				for (int x = 1; x < table.getRowCount() - 1; x++)
				{
					final HtmlTableRow row = table.getRow(x);
					
					if(row.getCells().size()<3)
						continue;
					
					String targetName = row.getCell(2).asText().replace("é", "�");
					
					if (!b.getName().contains(targetName))
					{
						System.out.println("Faux triage des ventes: "+targetName+" VS String:"+b.getName());
						continue;
					}
					
					
					String valS = row.getCell(0).asXml();
					try
					{
						valS = valS.split("value=\"")[1];
						valS = valS.split("\"/")[0];
						liste.add(Integer.parseInt(valS));
					}
					catch(ArrayIndexOutOfBoundsException e)
					{
						System.out.println("Fin de ligne: "+row.getCell(0).asXml());
					}
					
				}

				System.out.println("Selling: " + liste.size()+" "+b.getName());

				if (liste.size() > 0)
				{

					params = new ArrayList<NameValuePair>(2);
					params.add(new NameValuePair("id_propriete", ""+liste.get(0)));
					params.add(new NameValuePair("id_etat", "7"));
					
					page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/properties/properties_action.php"), params);
					Immobot.p.webClient.waitForBackgroundJavaScript(2000);

					
					String text = page.asText();
					text = Immobot.cleanString(text);
	
					text = text.split("100%\\(")[1].split("%desavaleur\\)")[0];
					double ratio = Integer.parseInt(text)/100.0;
					System.out.println("Current ratio: "+ratio);
					
					double profit = b.getProfitFromConstruction(ratio);

					if(profit<=0)
					{
						System.out.println("Not enought benefice with ratio: "+ratio+" benef: "+profit);
						double rationeeded = b.getConstructionTotalCost()*100/b.getPriceAchat();
						System.out.println("Need at least: "+Math.round(rationeeded)+" %");
						return;
					}
					else
					{
						System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
						System.out.println("Selling with PROFIT: "+NumberFormat.getNumberInstance(Locale.FRENCH).format(profit));
						System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
					}
					
				
					params = new ArrayList<NameValuePair>(2);

					String ids = "";

					boolean first = true;
					for (Integer id : liste)
					{
						if (first)
						{
							ids = id + "";
							first = false;
						}
						else
						{
							ids += ";" + id;
						}

					}

				
					
					params.add(new NameValuePair("id_propriete", ids));
					params.add(new NameValuePair("id_etat", "7"));
					params.add(new NameValuePair("confirmation", "1"));
					params.add(new NameValuePair("data", "1"));
					params.add(new NameValuePair("escalation", "0"));
					params.add(new NameValuePair("directdeal", "0"));
					params.add(new NameValuePair("promotor", "true"));
					params.add(new NameValuePair("oneEventOnly", "true"));

					page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/properties/properties_action.php"), params);
					Immobot.p.webClient.waitForBackgroundJavaScript(2000);

					if(page.asText().contains("En vente"))
					{
						System.out.println("Vente sucess !");
					}
					else
					{
						System.out.println("ERROR while vente ");
						System.out.println(page.asXml());
					}

				}
			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}

	
		
	}

}
